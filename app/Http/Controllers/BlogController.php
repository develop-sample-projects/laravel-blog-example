<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class BlogController extends Controller
{
    public function index(){
        $post = (object)[
            'id'=> '11',
            'title' => 'Lorem ipsum dolor sit amet.',
            'content' => 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Soluta, error.',
        ];
        $posts = array_fill(0, 11, $post);

        return view('blog.index', compact('posts'));
    }

    public function show($post){
        $post = (object)[
            'id'=> '11',
            'title' => 'Lorem ipsum dolor sit amet.',
            'content' => 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Soluta, error.',
        ];
        return view('blog.show', compact('post'));
    }

    public function like($post){
        return 'Поставить лайк';
    }
}

