<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PostController extends Controller
{
    public function index(){
        return 'Страница список постов';
    }

    public function create(){
        return 'Страница создания поста';
    }

    public function store(){
        return 'Запрос создания поста';
    }

    public function show($post){
        return "Страница показа поста {$post}";
    }

    public function edit(){
        return 'Страница изменения поста';
    }

    public function delete(){
        return 'Запрос на удаление поста';
    }

    public function like(){
        return 'Запрос на лайк';
    }

}
