<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PostController extends Controller
{
    public function index(){

        $post = (object)[
            'id'=> 11,
            'title' => 'Lorem ipsum dolor sit amet.',
            'content' => 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. SALUTA! error.',
        ];
        $posts = array_fill(0, 11, $post);

        return view('user.posts.index', compact('posts'));
    }

    public function create(){
        return view('user.posts.create');
    }

    public function store(){
        return 'Запрос создания поста';
    }

    public function show($post){
        $post = (object)[
            'id'=> 11,
            'title' => 'Lorem ipsum dolor sit amet.',
            'content' => 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. SALUTA! error.',
        ];
        return view('user.posts.show', compact('post'));
    }

    public function update(){
        return 'Страница обновления поста';
    }

    public function edit($post){
        $post = (object)[
            'id'=> 11,
            'title' => 'Lorem ipsum dolor sit amet.',
            'content' => 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. SALUTA! error.',
        ];
        return view('user.posts.edit', compact('post'));
    }

    public function delete(){
        return 'Запрос на удаление поста';
    }

    public function like(){
        return 'Запрос на лайк';
    }

}
