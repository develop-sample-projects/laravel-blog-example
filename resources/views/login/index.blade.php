{{-- Указываем какой шаблон расширяем --}}
@extends('layouts.auth')


@section('page.title')
    Страница входа
@endsection

@section('auth.content')
    <x-login.card />
@endsection
