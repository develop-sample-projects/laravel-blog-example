{{-- Указываем какой шаблон расширяем --}}
@extends('layouts.base')


@section('page.title')
    Страница входа
@endsection

@section('content')
    <main class="flex-grow-1 py-3">

        <section>
            <x-container>
                <div class="row">
                    <div class="col-12 col-md-6 offset-md-3">

                        @yield('auth.content')

                    </div>
                </div>
            </x-container>
        </section>

    </main>
@endsection
