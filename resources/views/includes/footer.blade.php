<footer class="py-3 border-top text-center">
    (C) {{config('app.name')}} {{ $date }}
</footer>