{{-- Указываем какой шаблон расширяем --}}
@extends('layouts.base')


@section('page.title')
    Регистрация
@endsection

@section('content')
    <main class="flex-grow-1 py-3">

        <section>
            <x-container>
                <div class="row">
                    <div class="col-12 col-md-6 offset-md-3">
                        <x-card>
                            <x-card-header>
                                <x-card-title>
                                    {{ __('Регистрация') }}
                                </x-card-title>
                                <x-slot name="right">
                                    <a href="{{ route('login') }}">
                                        {{ __('Вход') }}</a>
                                </x-slot>
                            </x-card-header>
                            <x-card-body>
                                <x-form action="{{ route('register.store') }}" method="POST">
                                    <x-form-item>
                                        <x-label required>
                                            {{ __('Имя') }}
                                        </x-label>
                                        {{-- <input type="email" name="email" class="form-control" autofocus> --}}
                                        <x-input name="name" autofocus />
                                    </x-form-item>

                                    <x-form-item>
                                        <x-label required>
                                            {{ __('Фамилия') }}
                                        </x-label>
                                        {{-- <input type="email" name="email" class="form-control" autofocus> --}}
                                        <x-input name="surname" autofocus />
                                    </x-form-item>

                                    <x-form-item>
                                        <x-label required>
                                            {{ __('Email') }}
                                        </x-label>
                                        {{-- <input type="email" name="email" class="form-control" autofocus> --}}
                                        <x-input type="email" name="email" autofocus />
                                    </x-form-item>

                                    <x-form-item>
                                        <x-label required>
                                            {{ __('Пароль') }}
                                        </x-label>
                                        <x-input type="password" name="password" />
                                    </x-form-item>

                                    <x-form-item>
                                        <x-label required>
                                            {{ __('Пароль еще раз') }}
                                        </x-label>
                                        <x-input type="password" name="password_confirmation" />
                                    </x-form-item>

                                    <x-form-item>
                                        <x-checkbox name="remember">
                                            {{ __('Я согласен на обработку персональных данных') }}
                                        </x-checkbox>
                                    </x-form-item>

                                    <x-button type="submit">
                                        {{ __('Войти') }}
                                    </x-button>
                                </x-form>
                            </x-card-body>
                        </x-card>
                    </div>
                </div>
            </x-container>
        </section>

    </main>
@endsection
