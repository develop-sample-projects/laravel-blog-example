@props(['name' => ''])

<input id="{{ $name }}" type="hidden" name="{{ $name }}">
<trix-editor input="{{ $name }}"></trix-editor>

@production
    @push('js')
        <script type="text/javascript" src="/js/trix.js"></script>
    @endpush

    @push('css')
        <link rel="stylesheet" type="text/css" href="/css/trix.css">
    @endpush
@endonce
