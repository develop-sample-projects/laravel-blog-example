<x-card>
    <x-card-body>
        <h2 class="h6">
            <a href="{{ route('blog.show', $foo->id) }}">{{ $foo->title }}</a> 
        </h2 class="h6">

        <div class="muted">
            {{ now()->format('d.m.y / H:m:s') }}
        </div>
        {{-- <p>{!! $foo->content !!}</p> --}}
    </x-card-body>
</x-card>
