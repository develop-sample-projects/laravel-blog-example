{{-- Указываем какой шаблон расширяем --}}
@extends('layouts.main')

@section('page.title', 'Изменить пост')

@section('main.content')
    <x-title>
        {{ __('Редактировать пост') }}

        <x-slot name="link">
            <a href="{{ route('user.posts.show', $post->id) }}">
                {{ __('Назад') }}
            </a>
        </x-slot>

    </x-title>

<x-post.form :post="$post"/>

@endsection

