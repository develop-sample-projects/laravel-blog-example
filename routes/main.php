<?php


use App\Http\Controllers\RegisterController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\Posts\CommentController;
use App\Http\Controllers\BlogController;
use App\Http\Controllers\PageController;

use App\Http\Middleware\LogMiddleware;


use Illuminate\Support\Facades\Route;

Route::view('/', 'home.index')->name('home');
Route::redirect('/home', '/', 308)->Middleware(LogMiddleware::class);
Route::get('/page', PageController::class)->name('page')->middleware('token:secret');

Route::middleware('guest')->group(function () {
    Route::get('/register', [RegisterController::class, 'index'])->name('register');
    Route::post('/register', [RegisterController::class, 'store'])->name('register.store');
    Route::get('/login', [LoginController::class, 'index'])->name('login');
    Route::post('/login', [LoginController::class, 'store'])->name('login.store');
});

// Route::get('/login/{$user}/confirmation', [LoginController::class, 'confirmation'])->name('login.confirmation');
// Route::post('/login/{$user}/confirm', [LoginController::class, 'confirm'])->name('confirm');

// user

Route::get('/blog', [BlogController::class, 'index'])->name('blog');
Route::get('/blog/{post}', [BlogController::class, 'show'])->name('blog.show');
Route::post('/blog/{post}', [BlogController::class, 'like'])->name('blog.like');

Route::resource('posts/{post}/comments', CommentController::class)->only([
    'index', 'show',
]);